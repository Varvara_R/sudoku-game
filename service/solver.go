package service

import (
	"fmt"
	"net/http"
	"sort"
	"sync/atomic"

	incoming "gitlab.com/Varvara_R/sudoku-game/api/incoming"
)

type Cell struct {
	rowIndex int8
	colIndex int8
	numOfCan int8
}

func isColumnDuplicate(board *incoming.RawGame, colIndex int8, i int8) bool {
	for j := 0; j < 9; j++ {
		v := board[j][colIndex]
		if v != 0 && v == i {
			return true
		}
	}
	return false
}

func isRowDuplicate(row incoming.RawRow, num int8) bool {
	for _, v := range row {
		if v == num {
			return true
		}
	}
	return false
}

func isSubgridDuplicate(board *incoming.RawGame, rowIndex int8, colIndex int8, v int8) bool {
	rowBound := (rowIndex / 3) * 3
	colBound := (colIndex / 3) * 3

	for i := rowBound; i < rowBound+3; i++ {
		for j := colBound; j < colBound+3; j++ {
			if board[i][j] == v {
				return true
			}
		}
	}
	return false
}

func hasEmptyCell(board *incoming.RawGame) bool {
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if board[i][j] == 0 {
				return true
			}
		}
	}
	return false
}

func getCandidates(grid *incoming.RawGame, rowIndex int8, columnIndex int8) []int8 {
	candidates := []int8{}
	var i int8
	for i = 1; i < 10; i++ {
		// check for duplicate in row
		if isRowDuplicate(grid[rowIndex], i) {
			continue
		}

		// check for duplicate in column
		if isColumnDuplicate(grid, columnIndex, i) {
			continue
		}

		// Check for duplicate 3*3 subgrid
		if isSubgridDuplicate(grid, rowIndex, columnIndex, i) {
			continue
		}

		candidates = append(candidates, i)
	}

	return candidates
}

func backTracking(grid *incoming.RawGame, history *[]incoming.RawGame, iterations *int32) bool {
	// store each step to visualise the process of algorithm
	*history = append(*history, *grid)

	// base case
	if !hasEmptyCell(grid) {
		return true
	}
	var i, j int8

	for i = 0; i < 9; i++ {
		for j = 0; j < 9; j++ {
			atomic.AddInt32(iterations, 1)
			if grid[i][j] == 0 {
				candidates := getCandidates(grid, i, j)

				if len(candidates) != 0 {
					for _, value := range candidates {
						grid[i][j] = value
						if backTracking(grid, history, iterations) {
							return true
						} else {
							grid[i][j] = 0
						}
					}
				}
				// current search path failed
				return false
			}
		}
	}
	// current search path failed
	return false
}

func backtrackingByCardinality(puzzle *incoming.RawGame, sortedList []Cell) bool {
	if !hasEmptyCell(puzzle) {
		return true
	}

	for _, v := range sortedList {
		if puzzle[v.rowIndex][v.colIndex] == 0 {
			candidates := getCandidates(puzzle, v.rowIndex, v.colIndex)

			for _, c := range candidates {
				puzzle[v.rowIndex][v.colIndex] = c
				if backtrackingByCardinality(puzzle, sortedList) {
					return true
				} else {
					puzzle[v.rowIndex][v.colIndex] = 0
				}
			}
			return false
		}
	}
	return false
}

func mapGripToSortedList(data incoming.RawGame) []Cell {
	list := []Cell{}

	for rowIndex, row := range data {
		for colIndex := range row {
			if data[rowIndex][colIndex] == 0 {
				candidates := getCandidates(&data, int8(rowIndex), int8(colIndex))

				cell := Cell{
					rowIndex: int8(rowIndex),
					colIndex: int8(colIndex),
					numOfCan: int8(len(candidates)),
				}

				list = append(list, cell)
			}
		}
	}

	sort.Slice(list, func(i, j int) bool {
		return list[i].numOfCan < list[j].numOfCan
	})

	return list
}

func Solve() (incoming.GameResponse, *RequestError) {
	rawGame, _, gameId, err := GetRandomGame()
	if err != nil {
		return incoming.GameResponse{}, MakeRequestError(http.StatusInternalServerError).New("Unable to get random game data")
	}

	var iterationCounter = new(int32)

	// board := [9][9]int{
	// 	{0, 0, 0, 5, 0, 0, 4, 2, 0},
	// 	{0, 5, 0, 0, 0, 9, 6, 0, 0},
	// 	{6, 8, 7, 0, 0, 0, 0, 1, 5},
	// 	{0, 0, 9, 6, 5, 8, 1, 3, 2},
	// 	{0, 0, 2, 0, 4, 0, 0, 0, 8},
	// 	{0, 0, 0, 0, 9, 1, 0, 6, 4},
	// 	{3, 0, 0, 0, 0, 2, 0, 0, 0},
	// 	{7, 2, 0, 0, 1, 0, 3, 4, 9},
	// 	{8, 9, 1, 0, 0, 7, 0, 5, 0},
	// }

	history := []incoming.RawGame{}
	backTracking(&rawGame, &history, iterationCounter)
	// sortedList := mapGripToSortedList(board)
	// backtrackingByCardinality(&board, sortedList)
	// fmt.Printf("%v", board)
	fmt.Printf("Total iterations: %v\n", int(*iterationCounter))
	// fmt.Printf("%v", history)
	gameResponse := mapSolutionGame(rawGame, gameId)
	return gameResponse, nil
}
