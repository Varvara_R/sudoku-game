package service

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"strings"
	"time"

	incoming "gitlab.com/Varvara_R/sudoku-game/api/incoming"
	"gitlab.com/Varvara_R/sudoku-game/common"
	"gitlab.com/Varvara_R/sudoku-game/mock"
)

func GetRandomGame() (incoming.RawGame, incoming.RawGame, int, *RequestError) {
	mockedRawGames := strings.NewReader(mock.RawGames)
	mockedSolvedGamed := strings.NewReader(mock.SolvedGames)
	var rawGames []incoming.RawGame
	var solvedGames []incoming.RawGame
	if err := json.NewDecoder(mockedRawGames).Decode(&rawGames); err != nil {
		return incoming.RawGame{}, incoming.RawGame{}, 0, MakeRequestError(http.StatusInternalServerError).From(err).Wrapf("Unable to decode mocked raw games")
	}
	if err := json.NewDecoder(mockedSolvedGamed).Decode(&solvedGames); err != nil {
		return incoming.RawGame{}, incoming.RawGame{}, 0, MakeRequestError(http.StatusInternalServerError).From(err).Wrapf("Unable to decode mocked solved games")
	}

	//randomize values to get always different game from 27 available games
	rand.Seed(time.Now().UnixNano())
	randNum := rand.Intn(27)
	rawGame := rawGames[randNum]
	solvedGame := solvedGames[randNum]

	return rawGame, solvedGame, randNum, nil
}

func GetRandomGameResponse() (incoming.GameResponse, *RequestError) {
	rawGame, solvedGame, randNum, err := GetRandomGame()

	if err != nil {
		return incoming.GameResponse{}, MakeRequestError(http.StatusInternalServerError).New("Unable to get random game data")
	}

	gameResponse := mapRawAndSolvedGame(rawGame, solvedGame, randNum)

	return gameResponse, nil
}

// private
func mapRawAndSolvedGame(rawGame, solvedGame incoming.RawGame, num int) incoming.GameResponse {
	gameResp := incoming.GameResponse{
		Id:         num,
		Difficulty: common.Light,
	}
	table := make([]incoming.GameGrid, 0)
	for rowId, row := range rawGame {
		rows := make([]incoming.GameCell, 0)
		for cellId, cell := range row {
			gameCell := incoming.GameCell{
				Digit:   solvedGame[rowId][cellId],
				IsFixed: cell != 0,
			}
			rows = append(rows, gameCell)
		}
		table = append(table, rows)
	}
	gameResp.Cells = table

	return gameResp
}

func mapSolutionGame(rawGame incoming.RawGame, gameId int) incoming.GameResponse {
	gameResp := incoming.GameResponse{
		Id:         gameId,
		Difficulty: common.Solved,
	}

	table := make([]incoming.GameGrid, 0)
	for _, row := range rawGame {
		rows := make([]incoming.GameCell, 0)
		for _, cell := range row {
			gameCell := incoming.GameCell{
				Digit:   cell,
				IsFixed: cell != 0,
			}
			rows = append(rows, gameCell)
		}
		table = append(table, rows)
	}
	gameResp.Cells = table

	return gameResp
}
