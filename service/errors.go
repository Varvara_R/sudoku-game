package service

import (
	"errors"
	"fmt"
)

type requestErrorConstructor struct {
	httpStatusCode int
}

type RequestError struct {
	Err            error
	HttpStatusCode int
}

func MakeRequestError(code int) *requestErrorConstructor {
	return &requestErrorConstructor{httpStatusCode: code}
}

func (c *requestErrorConstructor) New(message string) *RequestError {
	return &RequestError{Err: errors.New(message), HttpStatusCode: c.httpStatusCode}
}

func (c *requestErrorConstructor) From(err error) *RequestError {
	return &RequestError{Err: err, HttpStatusCode: c.httpStatusCode}
}

func (e *RequestError) Wrapf(format string, a ...interface{}) *RequestError {
	e.Err = fmt.Errorf(fmt.Sprintf(format, a...))
	return e
}

func (e *RequestError) Status(code int) *RequestError {
	e.HttpStatusCode = code
	return e
}
