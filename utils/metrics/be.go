package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
)

type beMetricsCounters interface {
	run(labelValues ...string)
	get() prometheus.Collector
}

type beMetricCounter struct {
	metric prometheus.Counter
}

func (m *beMetricCounter) run(labelValues ...string) {
	m.metric.Inc()
}

func (m *beMetricCounter) get() prometheus.Collector {
	return m.metric
}

type beMetricCounterVec struct {
	metric *prometheus.CounterVec
}

func (m *beMetricCounterVec) run(labelValues ...string) {
	m.metric.WithLabelValues(labelValues...).Inc()
}

func (m *beMetricCounterVec) get() prometheus.Collector {
	return m.metric
}

type optionalValues struct {
	Method      string
	LabelValues []string
}

type optionalValue func(*optionalValues)

func Method(method string) optionalValue {
	return func(args *optionalValues) {
		args.Method = method
	}
}

func LabelValues(labelValues ...string) optionalValue {
	return func(args *optionalValues) {
		args.LabelValues = labelValues
	}
}

func setOptionalValues(optional ...optionalValue) optionalValues {
	args := &optionalValues{
		Method: http.MethodGet,
	}

	for _, setter := range optional {
		setter(args)
	}
	return *args
}
