package metrics

import (
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

type UIEvent struct {
	Metric UIEventMetric `json:"metric"`
	Log    UIEventLog    `json:"log"`
}

type UIEventMetric struct {
	Name   string              `json:"name"`
	Labels UIEventMetricLabels `json:"labels"`
	Value  float64             `json:"value"`
}

type UIEventMetricLabels struct {
	Vital string `json:"vital"`
}

type UIEventLog struct {
	Level   string `json:"level"`
	Message string `json:"message"`
}

func RegisterUIMetrics() {
	if err := prometheus.Register(UIPageLoads); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_page_loads")
	} else {
		logrus.Info("ui_page_loads counter registered successfully")
	}

	if err := prometheus.Register(UINumberOfErrors); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_errors")
	} else {
		logrus.Info("ui_errors counter registered successfully")
	}

	if err := prometheus.Register(UIWebVitalsLCP); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_web_vitals_lcp")
	} else {
		logrus.Info("ui_web_vitals_lcp gauge registered successfully")
	}

	if err := prometheus.Register(UIWebVitalsFID); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_web_vitals_fid")
	} else {
		logrus.Info("ui_web_vitals_fid gauge registered successfully")
	}

	if err := prometheus.Register(UIWebVitalsCLS); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_web_vitals_cls")
	} else {
		logrus.Info("ui_web_vitals_cls gauge registered successfully")
	}
}

var (
	UIPageLoads = prometheus.NewCounter(prometheus.CounterOpts{
		Subsystem: "ui",
		Name:      "page_loads",
		Help:      "Number of page loads (from initial HTML)",
	})

	UINumberOfErrors = prometheus.NewCounter(prometheus.CounterOpts{
		Subsystem: "ui",
		Name:      "errors",
		Help:      "Number of errors ('error' and 'unhandledrejection' events)",
	})

	UIWebVitalsLCP = prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "ui",
		Name:      "web_vitals_lcp",
		Help:      "Largest Contentful Paint (LCP): measures loading performance",
	})

	UIWebVitalsFID = prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "ui",
		Name:      "web_vitals_fid",
		Help:      "First Input delay (FID): measures interactivity",
	})

	UIWebVitalsCLS = prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "ui",
		Name:      "web_vitals_cls",
		Help:      "Cumulative Layout Shift (CLS): measures visual stability",
	})
)

const (
	uIPageLoadsMetric = "ui_page_loads"
	uiWebVitalsMetric = "ui_web_vitals"
	uiErrorsMetric    = "ui_errors"

	uiWebVitalsLabelLCP = "lcp"
	uiWebVitalsLabelFID = "fid"
	uiWebVitalsLabelCLS = "cls"
)

func SendUIEvent(event UIEvent) error {
	switch event.Metric.Name {
	case uIPageLoadsMetric:
		UIPageLoads.Add(event.Metric.Value)
	case uiErrorsMetric:
		UINumberOfErrors.Add(event.Metric.Value)
		level, err := logrus.ParseLevel(event.Log.Level)
		if err != nil {
			return errors.Wrapf(err, "Unable to parse log level '%s", event.Log.Level)
		}
		uiError := errors.New(event.Log.Message)
		logrus.WithError(uiError).Log(level, "Incoming UI error")

	case uiWebVitalsMetric:
		switch event.Metric.Labels.Vital {
		case uiWebVitalsLabelLCP:
			UIWebVitalsLCP.Set(event.Metric.Value)
		case uiWebVitalsLabelFID:
			UIWebVitalsFID.Set(event.Metric.Value)
		case uiWebVitalsLabelCLS:
			UIWebVitalsCLS.Set(event.Metric.Value)
		}
	}

	return nil
}
