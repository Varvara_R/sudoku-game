package metrics

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

type beRequestMetrics struct {
	name                 string
	requestTime          prometheus.Gauge
	requestCounter       beMetricsCounters
	errorResponseCounter *prometheus.CounterVec
}

type beRequestMetricsCreator struct {
	name                 string
	key                  string
	method               string
	requestTime          prometheus.Gauge
	errorResponseCounter *prometheus.CounterVec
}

func createRequestMetrics(name, key, method string) *beRequestMetricsCreator {
	requestTime := prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "be",
		Name:      fmt.Sprintf("request_time_%s", name),
		Help:      fmt.Sprintf("Duration of %s request to %s, ms", method, key),
	})

	errorResponseCounter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Subsystem: "be",
		Name:      fmt.Sprintf("error_response_counter_%s", name),
		Help:      fmt.Sprintf("Error response counter of %s request to %s, times got", method, key),
	}, []string{"code"})

	return &beRequestMetricsCreator{
		name:                 name,
		key:                  key,
		method:               method,
		requestTime:          requestTime,
		errorResponseCounter: errorResponseCounter,
	}
}

func (c *beRequestMetricsCreator) counter() beRequestMetrics {
	requestCounter := prometheus.NewCounter(prometheus.CounterOpts{
		Subsystem: "be",
		Name:      fmt.Sprintf("request_counter_%s", c.name),
		Help:      fmt.Sprintf("Request counter of %s request to %s, times requested", c.method, c.key),
	})

	counter := &beMetricCounter{requestCounter}
	return beRequestMetrics{
		name:                 c.name,
		requestTime:          c.requestTime,
		requestCounter:       counter,
		errorResponseCounter: c.errorResponseCounter,
	}
}

func (c *beRequestMetricsCreator) counterVec(labelNames []string) beRequestMetrics {
	requestCounterVec := prometheus.NewCounterVec(prometheus.CounterOpts{
		Subsystem: "be",
		Name:      fmt.Sprintf("request_counter_%s", c.name),
		Help:      fmt.Sprintf("Request counter of %s request to %s, times requested", c.method, c.key),
	}, labelNames)

	counterVec := &beMetricCounterVec{requestCounterVec}
	return beRequestMetrics{
		name:                 c.name,
		requestTime:          c.requestTime,
		requestCounter:       counterVec,
		errorResponseCounter: c.errorResponseCounter,
	}
}

func (m *beRequestMetrics) register() {
	if err := prometheus.Register(m.requestTime); err != nil {
		logrus.WithError(err).Fatal("Unable to register be_request_time_%s", m.name)
	} else {
		logrus.Debugf("be_request_time_%s gauge is registered successfully", m.name)
	}

	if err := prometheus.Register(m.requestCounter.get()); err != nil {
		logrus.WithError(err).Fatal("Unable to register be_request_counter_%s", m.name)
	} else {
		logrus.Debugf("be_request_counter_%s counter is registered successfully", m.name)
	}

	if err := prometheus.Register(m.errorResponseCounter); err != nil {
		logrus.WithError(err).Fatal("Unable to register be_error_response_counter_%s", m.name)
	} else {
		logrus.Debugf("be_error_response_counter_%s error response is registered successfully", m.name)
	}
}

var (
	beGetNewGame  = createRequestMetrics("get_new_game", BERequestMetricsGetNewGameRouteKey, http.MethodGet).counter()
	beGetUserInfo = createRequestMetrics("get_user_info", BERequestMetricsGetUserRouteKey, http.MethodGet).counter()
	beGetSolution = createRequestMetrics("get_solution", BERequestMetricsGetSolutionRouteKey, http.MethodGet).counter()
	bePostEvents  = createRequestMetrics("post_events", BERequestMetricsPostEventsRouteKey, http.MethodPost).counter()
	beGetMetrics  = createRequestMetrics("get_metrics", BERequestMetricsGetMetricsRouteKey, http.MethodGet).counter()
)

const (
	BERequestMetricsGetNewGameRouteKey  = "/api/newGame"
	BERequestMetricsGetUserRouteKey     = "/api/user"
	BERequestMetricsGetSolutionRouteKey = "/api/solve"
	BERequestMetricsPostEventsRouteKey  = "/api/events"
	BERequestMetricsGetMetricsRouteKey  = "/metrics"
)

func RegisterBeMetrics() {
	beGetNewGame.register()
	beGetUserInfo.register()
	beGetSolution.register()
	bePostEvents.register()
	beGetMetrics.register()
}

func selectMetrics(key string, optional ...optionalValue) *beRequestMetrics {
	args := setOptionalValues(optional...)
	method := args.Method

	switch key {
	case BERequestMetricsGetNewGameRouteKey:
		return &beGetNewGame
	case BERequestMetricsGetUserRouteKey:
		return &beGetUserInfo
	case BERequestMetricsGetSolutionRouteKey:
		return &beGetSolution
	case BERequestMetricsPostEventsRouteKey:
		return &bePostEvents
	case BERequestMetricsGetMetricsRouteKey:
		return &beGetMetrics
	default:
		logrus.Warnf("No metrics for the route method: %s key: %s found", method, key)
		return nil
	}
}

func SendDuration(key string, start time.Time, optional ...optionalValue) {
	duration := time.Now().Sub(start)
	metrics := selectMetrics(key, optional...)
	if metrics != nil {
		metrics.requestTime.Set(duration.Seconds() * 1000)
	}
}

func IncrementRequestCounter(key string, optional ...optionalValue) {
	labelValues := make([]string, 0)
	args := setOptionalValues(optional...)
	metrics := selectMetrics(key, Method(args.Method), LabelValues(args.LabelValues...))
	if len(args.LabelValues) > 0 {
		labelValues = args.LabelValues
	}
	if metrics != nil {
		metrics.requestCounter.run(labelValues...)
	}
}

func IncrementErrorResponseCounter(key string, code int, optional ...optionalValue) {
	args := setOptionalValues(optional...)
	metrics := selectMetrics(key, Method(args.Method), LabelValues(args.LabelValues...))
	if metrics != nil {
		metrics.errorResponseCounter.WithLabelValues(strconv.Itoa(code)).Inc()
	}
}
