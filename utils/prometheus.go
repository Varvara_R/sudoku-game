package utils

import "gitlab.com/Varvara_R/sudoku-game/utils/metrics"

func RegisterMetrics() {
	metrics.RegisterUIMetrics()
	metrics.RegisterBeMetrics()
}
