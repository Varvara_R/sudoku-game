package utils

import (
	"time"

	incoming "gitlab.com/Varvara_R/sudoku-game/api/incoming"
)

func IsEventDateStartedAndNotFinished(period *incoming.Period) bool {
	const layoutISO = "2006-01-02"
	loc, _ := time.LoadLocation("Europe/Berlin")
	now := time.Now().In(loc)
	currentDate := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, loc)
	periodDateFrom, _ := time.ParseInLocation(layoutISO, period.DateFrom, loc)
	periodDateTo, _ := time.ParseInLocation(layoutISO, period.DateTo, loc)
	return (currentDate.After(periodDateFrom) || currentDate.Equal(periodDateFrom)) && (currentDate.Before(periodDateTo) || currentDate.Equal(periodDateTo))
}
