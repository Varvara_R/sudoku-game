package incoming

const (
	rows, columns = 9, 9
	empty         = 0
)

type RawGame [rows][columns]int8

type RawRow [rows]int8

type GameGrid []GameCell

type GameResponse struct {
	Id         int        `json:"id"`
	Difficulty string     `json:"difficulty"`
	Cells      []GameGrid `json:"cells"`
}

type GameCell struct {
	Digit   int8 `json:"digit"`
	IsFixed bool `json:"isFixed"`
}

type Period struct {
	DateFrom string `json:"dateFrom"`
	DateTo   string `json:"dateTo"`
}
