package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	incoming "gitlab.com/Varvara_R/sudoku-game/api/incoming"
	"gitlab.com/Varvara_R/sudoku-game/common"
	service "gitlab.com/Varvara_R/sudoku-game/service"
	"gitlab.com/Varvara_R/sudoku-game/utils"
	"gitlab.com/Varvara_R/sudoku-game/utils/metrics"
)

const (
	rows, columns = 9, 9
	empty         = 0
)

type Cell struct {
	digit   int8
	isFixed bool
}

type Grid [rows][columns]Cell

var (
	ErrBound      = errors.New("out of grid")
	ErrDigit      = errors.New("wrong digit")
	ErrInRow      = errors.New("already exist in a row")
	ErrInColumn   = errors.New("already exist in a column")
	ErrInRegion   = errors.New("already exist in a region")
	ErrFixedDigit = errors.New("impossible to redraw initial digits")
)

func NewSudoku(digits [rows][columns]int8) *Grid {
	var grid Grid
	for r := 0; r < rows; r++ {
		for c := 0; c < columns; c++ {
			d := digits[r][c]
			if d != empty {
				grid[r][c].digit = d
				grid[r][c].isFixed = true
			}
		}
	}
	return &grid
}

func (g *Grid) Set(row, column int, digit int8) error {
	switch {
	case !inBounds(row, column):
		return ErrBound
	case !validDigit(digit):
		return ErrDigit
	case g.isFixed(row, column):
		return ErrFixedDigit
	case g.inRow(row, digit):
		return ErrInRow
	case g.inColumn(column, digit):
		return ErrInColumn
	case g.inRegion(row, column, digit):
		return ErrInRegion
	}
	g[row][column].digit = digit
	return nil
}

func inBounds(row, column int) bool {
	if row < 0 || row >= rows || column < 0 || column >= columns {
		return false
	}
	return true
}

func validDigit(digit int8) bool {
	return digit >= 1 && digit <= 9
}

func (g *Grid) isFixed(row, column int) bool {
	return g[row][column].isFixed
}

func (g *Grid) inRow(row int, digit int8) bool {
	for c := 0; c < columns; c++ {
		if g[row][c].digit == digit {
			return true
		}
	}
	return false
}

func (g *Grid) inColumn(column int, digit int8) bool {
	for r := 0; r < rows; r++ {
		if g[r][column].digit == digit {
			return true
		}
	}
	return false
}

func (g *Grid) inRegion(row, column int, digit int8) bool {
	startRow, startColumn := row/3*3, column/3*3
	for r := startRow; r < startRow+3; r++ {
		for c := startColumn; c < startColumn+3; c++ {
			if g[r][c].digit == digit {
				return true
			}
		}
	}
	return false
}

func (g *Grid) Clear(row, column int) error {
	switch {
	case !inBounds(row, column):
		return ErrBound
	case g.isFixed(row, column):
		return ErrFixedDigit
	}
	g[row][column].digit = empty
	return nil
}

func GetUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	defer metrics.SendDuration(metrics.BERequestMetricsGetUserRouteKey, time.Now())
	metrics.IncrementRequestCounter(metrics.BERequestMetricsGetUserRouteKey)

	var user incoming.User
	user.FirstName = strings.ReplaceAll(ps.ByName("name"), ":", "")
	user.LastName = "Eva"

	result, err := json.Marshal(user)

	if err != nil {
		logrus.Error(err, "Unable to marshal get user response")
		http.Error(w, "Unable to marshal get user response", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetUserRouteKey, http.StatusInternalServerError)
		return
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	_, err = fmt.Fprintf(w, "%s", result)

	if err != nil {
		logrus.Error(err, "Unable to write get user response in response writer")
		http.Error(w, "Unable to write get user response in response writer", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetUserRouteKey, http.StatusInternalServerError)
		return
	}
}

func GetNewGame(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	defer metrics.SendDuration(metrics.BERequestMetricsGetNewGameRouteKey, time.Now())
	metrics.IncrementRequestCounter(metrics.BERequestMetricsGetNewGameRouteKey)

	response, responseErr := service.GetRandomGameResponse()
	if responseErr != nil {
		logrus.Error(responseErr, "Unable to get random game")
		http.Error(w, "Unable to get random game", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetNewGameRouteKey, http.StatusInternalServerError)
		return
	}

	result, err := json.Marshal(response)

	if err != nil {
		logrus.Error(err, "Unable to marshal get newGame response")
		http.Error(w, "Unable to marshal get newGame response", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetNewGameRouteKey, http.StatusInternalServerError)
		return
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
	_, err = fmt.Fprintf(w, "%s", result)

	if err != nil {
		logrus.Error(err, "Unable to write get newGame response in response writer")
		http.Error(w, "Unable to write get newGame response in response writer", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetNewGameRouteKey, http.StatusInternalServerError)
		return
	}
}

func GetSolution(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	defer metrics.SendDuration(metrics.BERequestMetricsGetSolutionRouteKey, time.Now())
	defer utils.Timer("get_solution")()
	metrics.IncrementRequestCounter(metrics.BERequestMetricsGetSolutionRouteKey)

	response, responseErr := service.Solve()
	if responseErr != nil {
		logrus.Error(responseErr, "Unable to get solution of game")
		http.Error(w, "Unable to get solution of game", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetSolutionRouteKey, http.StatusInternalServerError)
		return
	}

	result, err := json.Marshal(response)

	if err != nil {
		logrus.Error(err, "Unable to marshal get solution response")
		http.Error(w, "Unable to marshal get solution response", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetSolutionRouteKey, http.StatusInternalServerError)
		return
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
	_, err = fmt.Fprintf(w, "%s", result)

	if err != nil {
		logrus.Error(err, "Unable to write get solution response in response writer")
		http.Error(w, "Unable to write get solution response in response writer", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsGetSolutionRouteKey, http.StatusInternalServerError)
		return
	}
}

func PostEvents(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer metrics.SendDuration(metrics.BERequestMetricsPostEventsRouteKey, time.Now())
	metrics.IncrementRequestCounter(metrics.BERequestMetricsPostEventsRouteKey)

	contentType := r.Header.Get(common.ContentType)
	if contentType != common.ApplicationJSON {
		err := errors.New("Unsupported media type of POST events")
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusUnsupportedMediaType)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsPostEventsRouteKey, http.StatusUnsupportedMediaType)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsPostEventsRouteKey, http.StatusInternalServerError)
		return
	}

	var events []metrics.UIEvent
	err = json.Unmarshal(body, &events)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsPostEventsRouteKey, http.StatusInternalServerError)
		return
	}

	err = metrics.SendUIEvent(events[0])
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(metrics.BERequestMetricsPostEventsRouteKey, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
func GetMetrics(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	defer metrics.SendDuration(metrics.BERequestMetricsGetMetricsRouteKey, time.Now())
	metrics.IncrementRequestCounter(metrics.BERequestMetricsGetMetricsRouteKey)
	promhttp.Handler().ServeHTTP(w, r)
}

func main() {
	router := httprouter.New()
	router.GET("/api/user/:name", GetUser)
	router.GET("/api/newGame", GetNewGame)
	router.GET("/api/solve", GetSolution)
	router.GET("/api/events", PostEvents)
	router.GET("/metrics", GetMetrics)
	utils.RegisterMetrics()

	port := ":8080"
	logrus.Printf("Server has started successfully, listening on %s \n", port)
	http.ListenAndServe(port, router)
}
