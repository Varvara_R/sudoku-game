package common

const (
	//Header names
	Accept                   = "Accept"
	ContentType              = "Content-type"
	ApplicationJSON          = "application/json"
	AccessControlAllowOrigin = "Access-Control-Allow-Origin"
	Asterisk                 = "*"

	// game types
	Light       = "light"
	Difficult   = "difficult"
	Challenging = "challenging"
	Solved      = "solved"
)
